IDIR = include
ODIR = obj
SDIR = src

CC = gcc
CFLAGS = -g -Wall -std=c11 -I$(IDIR)
#CFLAGS = -g -Wall -Wextra -std=c11 -I$(IDIR)

PROG = dijkstra

_DEPS = list.h town.h road.h graph.h test.h tree.h dyntable.h heap.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ= list.o town.o road.o graph.o test.o main.o tree.o dyntable.o heap.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

.PHONY: run all remake clean delete

all : $(PROG)

$(PROG) : $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

$(ODIR)/%.o : $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

run : all
	./$(PROG)

clean :
	rm -f $(ODIR)/*.o

delete : clean
	rm $(PROG)
