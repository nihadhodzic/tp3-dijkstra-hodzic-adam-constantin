#ifndef _TEST_H_
#define _TEST_H_

void test_list();

void test_list_assert();

void test_listRemoveFirst();

void test_listRemoveLast();

void test_listRemoveNode();

void test_listSwap();

void test_printTNodeInt(const void * data);

void test_CBTree();

void test_CBTree_assert();

void test_realloc();

void test_dyntable();

void test_heap(int heaptype);

#endif // _TEST_H_
