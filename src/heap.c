#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"

HNode * newHNode(int value, void *data) {
	HNode * node = malloc(sizeof(HNode));
	node->data = data;
	node->value = value;
	return node;
}

// type =
//    0 (DynTableHeap)
//    1 (CompleteBinaryTreeHeap)
//    2 (ListHeap)
Heap * newHeap(int type) {
	assert(type == 0 || type == 1 || type == 2);
	Heap* H = malloc(1, sizeof(Heap));
	H->dict = newDTable();
	switch (type) {
		case 0:
			H->heap = newDTable();
			H->HeapInsert = DTHeapInsert;
			H->HeapExtractMin = DTHeapExtractMin;
			H->HeapIncreasePriority = DTHeapIncreasePriority;
			H->viewHeap = viewDTHeap;
			break;
		case 1:
			H->heap = newCBTree();
			H->HeapInsert = CBTHeapInsert;
			H->HeapExtractMin = CBTHeapExtractMin;
			H->HeapIncreasePriority = CBTHeapIncreasePriority;
			H->viewHeap = viewCBTHeap;
			break;
		case 2:
			H->heap = newList();
			H->HeapInsert = OLHeapInsert;
			H->HeapExtractMin = OLHeapExtractMin;
			H->HeapIncreasePriority = OLHeapIncreasePriority;
			H->viewHeap = viewOLHeap;
			break;
	}
	return H;
}

/**********************************************************************************
 * DYNAMIC TABLE HEAP
 **********************************************************************************/

void* DTHeapInsert(Heap *H, int value, void *data) {
	DTable * heap = H->heap;

	int * iheap = malloc(sizeof(int));
	int * idict = malloc(sizeof(int));

	*iheap = heap->used;
	*idict = H->dict->used;

	HNode * node = newHNode(value, data);
	node->ptr = idict;

	DTableInsert(heap, node);
	DTableInsert(H->dict, iheap);

	for(int i = *iheap, iparent; i != 0; i = iparent) {
		iparent = (i - 1) / 2;
		HNode * parent = heap->T[iparent];

		if(value < parent->value) {
			DTableSwap(heap, iparent, i);
			DTableSwap(H->dict, *(int*)parent->ptr, *idict);
		} else break;
	}

	return idict;
}

HNode * DTHeapExtractMin(Heap* H) {
	DTable * heap = H->heap;
	DTableSwap(heap, 0, heap->used - 1);

	HNode * min = DTableRemove(heap);
	HNode * parent = heap->T[0];

	int idict = *(int*)parent->ptr;
	DTableSwap(H->dict, *(int*)min->ptr, idict);

	int i = 0, iright, ileft;
	while((ileft = (2 * i + 1)) < heap->used) {
		iright = ileft + 1;
		HNode * left = heap->T[ileft];
		if(iright != heap->used) {
			HNode * right = heap->T[iright];
			if(right->value < left->value) {
				if(right->value < parent->value) {
					DTableSwap(heap, i, iright);
					DTableSwap(H->dict, idict, *(int*)right->ptr);
					i = iright;
					continue;
				} else break;
			}
		}

		if(left->value < parent->value) {
			DTableSwap(heap, i, ileft);
			DTableSwap(H->dict, idict, *(int*)left->ptr);
			i = ileft;
		} else break;
	}

	free(H->dict->T[*(int*)min->ptr]);
	free(min->ptr);
	return min;
}

void DTHeapIncreasePriority(Heap* H, void *position, int value) {
	int idict = *(int*) position;
	int iheap = *(int*) H->dict->T[idict];

	DTable * heap = H->heap;
	((HNode*) heap->T[iheap])->value = value;

	for(int iparent; iheap != 0; iheap = iparent) {
		iparent = (iheap - 1) / 2;
		HNode * parent = heap->T[iparent];

		if(value < parent->value) {
			DTableSwap(heap, iparent, iheap);
			DTableSwap(H->dict, *(int*)parent->ptr, idict);
		} else break;
	}
}

void viewDTHeap(const Heap* H, void (*ptrf)(const void*)) {
	viewDTable(H->heap, ptrf);
}

/**********************************************************************************
 * COMPLETE BINARY TREE HEAP
 **********************************************************************************/

void* CBTHeapInsert(Heap *H, int value, void *data) {
	CBTree * heap = H->heap;
	CBTreeInsert(heap, newHNode(value, data));
	TNode * node = heap->last;

	while(node->parent && value < ((HNode*) node->parent->data)->value)
		CBTreeSwap(heap, node->parent, node);

	return node;
}

HNode * CBTHeapExtractMin(Heap *H) {
	CBTree * heap = H->heap;
	CBTreeSwapRootLast(heap);
	HNode* min = CBTreeRemove(heap);
	TNode* node = heap->root;

	if(node != NULL) {
		HNode * parent = node->data;
		while(node->left) {
			HNode * left = node->left->data;
			if(node->right != NULL) {
				HNode * right = node->right->data;
				if(right->value < left->value) {
					if(right->value < parent->value) {
						CBTreeSwap(heap, node, node->right);
						continue;
					} else break;
				}
			}

			if(left->value < parent->value)
				CBTreeSwap(heap, node, node->left);
			else break;
		}
	}
	
	return min;
}

void CBTHeapIncreasePriority(Heap *H, void *tnode, int value) {
	TNode* node = tnode;
	((HNode*)node->data)->value = value;

	while(node->parent && value < ((HNode*)node->parent->data)->value)
		CBTreeSwap(H->heap, node->parent, node);
}

void viewCBTHeap(const Heap *H, void (*ptrf)(const void*)) {
	viewCBTree(H->heap, ptrf, 0);
}

/**********************************************************************************
 * ORDERED-LIST HEAP
 **********************************************************************************/

void* OLHeapInsert(Heap *H, int value, void* data) {
	List * L = H->heap;
	LNode * iterator = L->head;
	HNode * node = newHNode(value, data);

	while(iterator && ((HNode*)iterator->data)->value < value)
		iterator = iterator->suc;

	if(iterator == NULL) {
		listInsertLast(L, node);
		return L->tail;
	}

	if(iterator->pred == NULL) {
		listInsertFirst(L, node);
		return L->head;
	}

	listInsertAfter(L, node, iterator->pred);
	return iterator->pred;
}

HNode * OLHeapExtractMin(Heap *H) {
	LNode * lnode = listRemoveFirst(H->heap);
	HNode * hnode = lnode->data;
	free(lnode);
	return hnode;
}

void OLHeapIncreasePriority(Heap *H, void* lnode, int value) {
	LNode * iterator = lnode;
	((HNode *)iterator->data)->value = value;

	while(iterator->pred && value < ((HNode*)iterator->pred->data)->value)
		listSwap(H->heap, iterator->pred, iterator);
}

void viewOLHeap(const Heap *H, void (*ptrf)()) {
	viewList(H->heap, ptrf);
}
