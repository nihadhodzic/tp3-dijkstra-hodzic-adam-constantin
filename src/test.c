#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "list.h"
#include "tree.h"
#include "dyntable.h"
#include "heap.h"
#include "assert.h"


static int compare_lists(List *l1, int l2[], int size) {
	if (l1->numelm != size)
		return 0;

	LNode *curr = l1->head;
	int i = 0;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->suc;
		i++;
	}

	curr = l1->tail;
	i = size-1;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->pred;
		i--;
	}
	return 1;
}

void test_list() {
	int t = 0;
	int f = 0;

	fprintf(stderr,"Testing list functions ");

	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;

	int tab[4] = {*i1,*i2,*i3,*i4};
	int t1[1] = {*i1};
	int t2[2] = {*i2,*i1};
	int t3[3] = {*i3,*i2,*i1};
	int t4[4] = {*i4,*i3,*i2,*i1};

	List *L = newList();
	listInsertLast(L, (int*) i1);
	if (compare_lists(L, tab, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i2);
	if (compare_lists(L, tab, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i3);
	if (compare_lists(L, tab, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i4);
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	deleteList(L, NULL);
	if (compare_lists(L, tab, 0) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	listInsertFirst(L, (int*) i1);
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i2);
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i3);
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i4);
	if (compare_lists(L, t4, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
}

void print_ptr_int(void * ptr) {
	printf("%d", *(int*)ptr);
}

void test_list_assert() {
	List* L = newList();

	int * d1 = malloc(sizeof(int));
	int * d2 = malloc(sizeof(int));
	int * d3 = malloc(sizeof(int));
	int * d4 = malloc(sizeof(int));

	listInsertLast(L, d1);
	listInsertLast(L, d2);
	listInsertLast(L, d3);
	listInsertLast(L, d4);

	listSwap(L, L->head, L->head->suc);
	// [1, 2, 3, 4] => [2, 1, 3, 4]
	assert(L->head->data == d2 && L->head->suc->data == d1 && L->head->pred == NULL && L->head->suc->suc->data == d3);
	listSwap(L, L->tail->pred, L->tail);
	// 2, 1, 3, 4 => 2, 1, 4, 3
	assert(L->tail->data == d3 && L->tail->pred->data == d4 && L->tail->suc == NULL && L->tail->pred->pred->data == d1);

	listSwap(L, L->head->suc, L->tail->pred);
	// 2, 1, 4, 3 => 2, 4, 1, 3
	assert(L->head->suc->data == d4 && L->tail->pred->data == d1 && L->head->suc->pred->data == d2 && L->tail->pred->suc->data == d3);

	listRemoveFirst(L);
	// 4, 1, 3
	assert(L->numelm == 3 && L->head->data == d4 && L->head->pred == NULL);

	listRemoveLast(L);
	// 4, 1
	assert(L->numelm == 2 && L->tail->data == d1 && L->tail->suc == NULL);

	listInsertLast(L, d2);
	// 4, 1, 2

	listRemoveNode(L, L->head->suc);
	// 4, 2
	assert(L->numelm == 2 && L->head->data == d4 && L->tail->data == d2 && L->head->suc == L->tail && L->tail->pred == L->head);

	listRemoveNode(L, L->head);
	// 2
	assert(L->numelm == 1 && L->head->data == d2 && L->head->pred == NULL && L->head->suc == NULL && L->head == L->tail);

	listRemoveNode(L, L->head);
	// vide
	assert(L->numelm == 0 && L->head == NULL && L->tail == NULL);

	printf("All functions from 'list.c' work fine.\n");

}

void test_listRemoveFirst() {
	printf("\nTesting listRemoveFirst():\n");
	List * L;
	int * x;

	for(int i = 2; i < 5; i++ ) {
		
		L = newList();

		for(int j = 1; j < i; j++) {
			x = malloc(sizeof(int));
			*x = j;
			listInsertLast(L, x);
		}

		printf("La liste n°%d: [", i-1);

		for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
			printf("%d", *((int*)iterator->data));

			if(iterator->suc != NULL) {
				printf(",");
			}
		}

		listRemoveFirst(L);

		printf("]\nLa liste n°%d après listRemoveFirst(): [", i-1);

		for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
			printf("%d", *((int*)iterator->data));

			if(iterator->suc != NULL) {
				printf(",");
			}
		}

		printf("]\n");
	}
}

void test_listRemoveLast() {
	printf("\nTesting listRemoveLast():\n");
	List * L;
	int * x;

	for(int i = 2; i < 5; i++ ) {
		
		L = newList();

		for(int j = 1; j < i; j++) {
			x = malloc(sizeof(int));
			*x = j;
			listInsertLast(L, x);
		}

		printf("La liste n°%d: [", i-1);

		for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
			printf("%d", *((int*)iterator->data));

			if(iterator->suc != NULL) {
				printf(",");
			}
		}

		listRemoveLast(L);

		printf("]\nLa liste n°%d après listRemoveLast(): [", i-1);

		for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
			printf("%d", *((int*)iterator->data));

			if(iterator->suc != NULL) {
				printf(",");
			}
		}

		printf("]\n");
	}
}

void test_listRemoveNode() {
	printf("\nTesting listRemoveNode():\n");
	int * x1 = malloc(sizeof(int)),
		* x2 = malloc(sizeof(int)),
		* x3 = malloc(sizeof(int));

	*x1 = 2;
	*x2 = 4;
	*x3 = 6;

	List * L = newList();

	listInsertLast(L, x1);
	listInsertLast(L, x2);
	listInsertLast(L, x3);

	printf("La liste définie:\n\t[");
	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}


	listRemoveNode(L, L->head->suc);

	printf("]\nLa liste après listRemoveNode(Liste,4):\n\t[");

	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	listRemoveNode(L, L->head->suc);

	printf("]\nLa liste après listRemoveNode(Liste,6):\n\t[");

	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	listRemoveNode(L, L->head);

	printf("]\nLa liste après listRemoveNode(Liste,2):\n\t[");

	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	printf("]\n");
}

void test_listSwap() {
	printf("\nTesting listSwap():\n");
	int * x1 = malloc(sizeof(int)),
		* x2 = malloc(sizeof(int)),
		* x3 = malloc(sizeof(int));

	*x1 = 2;
	*x2 = 4;
	*x3 = 6;

	List * L = newList();

	listInsertLast(L, x1);
	listInsertLast(L, x2);
	listInsertLast(L, x3);

	printf("La liste définie:\n\t[");
	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	listSwap(L, L->head->suc, L->head->suc->suc);

	printf("]\nLa liste après listSwap(Liste,4,6):\n\t[");

	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	listSwap(L, L->head, L->head->suc);

	printf("]\nLa liste après listSwap(Liste,2,6):\n\t[");

	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	listRemoveLast(L);
	listSwap(L, L->head, L->tail);

	printf("]\nOn supprime le 4 et on va tester listSwap(Liste,6,2):\n\t[");

	for(LNode * iterator = L->head; iterator ; iterator = iterator->suc) {
		printf("%d", *((int*)iterator->data));

		if(iterator->suc != NULL)
			printf(",");
	}

	printf("]\n\n");
}


void test_CBTree_assert() {

	printf("Testing tree.c functions...\n");
	CBTree * tree = newCBTree();
	assert(tree->root == NULL && tree->last == NULL && tree->numelm == 0);

	int * d1 = malloc(sizeof(int));
	TNode * n1 = newTNode(d1);
	assert(n1->data == d1 && n1->left == NULL && n1->right == NULL && n1->parent == NULL);
	free(n1);

	int * d2 = malloc(sizeof(int));
	int * d3 = malloc(sizeof(int));
	int * d4 = malloc(sizeof(int));
	int * d5 = malloc(sizeof(int));

	CBTreeInsert(tree, d1);
	assert(tree->numelm == 1 && tree->root->data == d1 && tree->last->data == d1);

	CBTreeInsert(tree, d2);
	assert(tree->numelm == 2 && tree->last->data == d2);
	assert(tree->root->left == tree->last && tree->last->parent == tree->root);

	CBTreeInsert(tree, d3);
	assert(tree->numelm == 3 && tree->last->data == d3);
	assert(tree->root->right == tree->last && tree->last->parent == tree->root);

	CBTreeInsert(tree, d4);
	assert(tree->numelm == 4 && tree->last->data == d4);
	assert(tree->root->left->left == tree->last && tree->last->parent->data == d2);

	CBTreeInsert(tree, d5);
	assert(tree->numelm == 5 && tree->last->data == d5);
	assert(tree->root->left->right == tree->last && tree->last->parent->data == d2);

	CBTreeSwap(tree, tree->root, tree->root->left);
	assert(tree->root->data == d2 && tree->root->left->data == d1);

	CBTreeSwap(tree, tree->root->left, tree->last);
	assert(tree->root->left->data == d5 && tree->last->data == d1);

	CBTreeSwapRootLast(tree);
	assert(tree->root->data == d1 && tree->last->data == d2);

	CBTreeRemove(tree);
	assert(tree->numelm == 4 && tree->last->data == d4);

	CBTreeRemove(tree);
	assert(tree->numelm == 3 && tree->last->data == d3);

	CBTreeSwapRootLast(tree);
	assert(tree->root->data == d3 && tree->last->data == d1);

	CBTreeRemove(tree);
	assert(tree->numelm == 2 && tree->last->data == d5);

	CBTreeRemove(tree);
	assert(tree->numelm == 1 && tree->last->data == d3);

	CBTreeRemove(tree);
	assert(tree->numelm == 0 && tree->root == NULL && tree->last == NULL);

	printf("All functions from tree.c work fine.\n");
}

void test_printTNodeInt(const void * data) {
	printf("%d", *(int*)data);
}

void test_CBTree() {

	CBTree* A = newCBTree();

	void (*ptrF)(const void*);
	ptrF = &test_printTNodeInt;

	int* Tptr[8];
	int i;
	for (i=0;i<=8;i++) {
		Tptr[i] = (int*)malloc(sizeof(int));
		*Tptr[i] = i+1;
		CBTreeInsert(A, Tptr[i]);
	}
		printf("Parcours préfixé : ");
		viewCBTree(A, (*ptrF), 0);
		printf("Parcours infixé : ");
		viewCBTree(A, (*ptrF), 2);
		printf("Parcours postfixé : ");
		viewCBTree(A, (*ptrF), 1);
}

void test_realloc() {
	int *T = realloc(NULL, 5*sizeof(int));
	for (int i = 0; i < 5; i++) *(T+i)=10+i;
	for (int i = 0; i < 5; i++) printf("%d \n", *(T+i));
	T = realloc(T, 10*sizeof(int));
	for (int i = 5; i < 10; i++) *(T+i)=10+i;
	for (int i = 0; i < 10; i++) printf("%d \n", *(T+i));
}

void test_dyntable() {
	DTable * T = newDTable();

	void (*ptrF)(const void*);
	ptrF = &test_printTNodeInt;

	int *E = malloc(5*sizeof(int));
	for (int i = 0; i < 5; i++) {
		*(E+i)=10+i;
		DTableInsert(T, E+i);
	}

	void * a = DTableRemove(T);
	void * b = DTableRemove(T);

	int *F = malloc(4*sizeof(int));
	for (int i = 0; i < 4; i++) {
		*(F+i)=100+i;
		DTableInsert(T, F+i);
	}

	viewDTable(T, (*ptrF));

	int size = T->used;

	for (int i = 0; i <= size-1; i++) {
		DTableRemove(T);
	}

	free(E);
	free(F);

	int *G = malloc(4*sizeof(int));
	for (int i = 0; i < 4; i++) {
		*(G+i)=3+i;
		DTableInsert(T, G+i);
	}

	DTableInsert(T, b);
	DTableInsert(T, a);

	viewDTable(T, (*ptrF));

	DTableSwap(T, 1, 5);

	viewDTable(T, (*ptrF));
}

void test_heap(int heaptype) {
	assert(heaptype >= 0 && heaptype <= 2);

	char * str_type = (heaptype == 0) ? "DTHeap" : (heaptype == 1) ? "CBTHeap" : "OLHeap";
	printf("Testing %s functions...\n", str_type);

	Heap* H = newHeap(heaptype);

	void * d1 = malloc(sizeof(int));
	void * d2 = malloc(sizeof(int));
	void * d3 = malloc(sizeof(int));
	void * d4 = malloc(sizeof(int));
	void * d5 = malloc(sizeof(int));

	int * t1 = DTHeapInsert(H, 1, d1);
	int	* t2 = DTHeapInsert(H, 6, d2);
	int	* t3 = DTHeapInsert(H, 4, d3);
	int	* t4 = DTHeapInsert(H, 8, d4);
	int	* t5 = DTHeapInsert(H, 5, d5);

	DTHeapIncreasePriority(H, t4, 2);
	DTHeapIncreasePriority(H, t2, 3);
	DTHeapIncreasePriority(H, t3, 0);

	assert(DTHeapExtractMin(H)->data == d3);
	assert(DTHeapExtractMin(H)->data == d1);
	assert(DTHeapExtractMin(H)->data == d4);
	assert(DTHeapExtractMin(H)->data == d2);
	assert(DTHeapExtractMin(H)->data == d5);

	printf("All %s functions work fine.\n", str_type);
}