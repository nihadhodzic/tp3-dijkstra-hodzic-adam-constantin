#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

List * newList() {
	List * L = calloc(1, sizeof(List));
	return L;
}

void deleteList(List * L, void (*ptrF)()) {
	LNode * iterator = L->head;

	if (ptrF == NULL) {
		while (iterator) {
			LNode * current = iterator;

			iterator = iterator->suc;
			free(current);
		}
	} else {
		while (iterator) {
			LNode * current = iterator;

			(*ptrF)(&current->data);
			free(current);
		}
	}
	L->head = NULL;
	L->tail = NULL;
	L->numelm = 0;
}

void viewList(const List * L, void (*ptrF)()) {
	printf("nb of nodes = %d\n", L->numelm);
	for(LNode * iterator = L->head; iterator; iterator = iterator->suc) {
		(*ptrF)(iterator->data);
		printf(" ");
	}
	printf("\n");
}

void listInsertFirst(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->suc = L->head;

	if(L->head == NULL)
		L->tail = E;
	else
		L->head->pred = E;
	L->head = E;
	L->numelm += 1;
}

void listInsertLast(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = L->tail;

	if(L->tail == NULL)
		L->head = E;
	else
		L->tail->suc = E;
	L->tail = E;
	L->numelm += 1;
}

void listInsertAfter(List * L, void * data, LNode * ptrelm) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = ptrelm;

	if(ptrelm == L->tail)
		L->tail = E;
	else
		ptrelm->suc->pred = E;
	E->suc = ptrelm->suc;
	ptrelm->suc = E;
	L->numelm += 1;
}

LNode* listRemoveFirst(List * L) {
	assert(L->head);

	LNode * firstnode = L->head;

	if((L->head = firstnode->suc) == NULL)
		L->tail = NULL;
	else
		L->head->pred = NULL;

	L->numelm -= 1;
	return firstnode;
}

LNode* listRemoveLast(List * L) {
	assert(L->head);

	LNode * lastnode = L->tail;

	if((L->tail = lastnode->pred) == NULL)
		L->head = NULL;
	else
		L->tail->suc = NULL;

	L->numelm -= 1;
	return lastnode;
}

LNode* listRemoveNode(List * L, LNode * node) {
	if (node->pred == NULL)
		return listRemoveFirst(L);

	if (node->suc == NULL)
		return listRemoveLast(L);

	node->pred->suc = node->suc;
	node->suc->pred = node->pred;
	L->numelm -= 1;
	return node;
}

void listSwap(List * L, LNode * left, LNode *right) {
	assert(left->suc == right && left == right->pred);

	if (right->suc == NULL)
		L->tail = left;
	else
		right->suc->pred = left;

	if (left->pred == NULL)
		L->head = right;
	else
		left->pred->suc = right;

	left->suc = right->suc;
	right->pred = left->pred;
	left->pred = right;
	right->suc = left;
}