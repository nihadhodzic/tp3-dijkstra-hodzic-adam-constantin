#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tree.h"

TNode * newTNode(void* data) {
	TNode* node = malloc(sizeof(TNode));
	node->data = data;
	node->left = NULL;
	node->right = NULL;
	node->parent = NULL;
	return node;
}

CBTree * newCBTree() {
	return calloc(1, sizeof(CBTree));
}

static void preorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		ptrF(node->data);
		printf(" ");
		preorder(node->left, ptrF);
		preorder(node->right, ptrF);
	}
}

static void inorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		inorder(node->left, ptrF);
		ptrF(node->data);
		printf(" ");
		inorder(node->right, ptrF);
	}
}

static void postorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		postorder(node->left, ptrF);
		postorder(node->right, ptrF);
		ptrF(node->data);
		printf(" ");
	}
}

// order = 0 (preorder), 1 (postorder), 2 (inorder)
void viewCBTree(const CBTree* tree, void (*ptrF)(const void*), int order) {
	assert(order == 0 || order == 1 || order == 2);
	printf("nb of nodes = %d\n", tree->numelm);
	switch (order) {
		case 0:
			preorder(tree->root, ptrF);
			break;
		case 1:
			postorder(tree->root, ptrF);
			break;
		case 2:
			inorder(tree->root, ptrF);
		break;
	}
	printf("\n");
}

void CBTreeInsert(CBTree* tree, void* data) {
	TNode * node = newTNode(data);

	if(tree->root == tree->last) {
		if(tree->root == NULL)
			tree->root = node;
		else {
			tree->root->left = node;
			node->parent = tree->root;
		}
	} else {
		TNode * iterator = tree->last->parent;

		if(iterator->right == NULL) {
			iterator->right = node;
			node->parent = iterator;

		} else {
			while(iterator->parent && iterator == iterator->parent->right)
				iterator = iterator->parent;

			if(iterator->parent != NULL)
				iterator = iterator->parent->right;

			while(iterator->left)
				iterator = iterator->left;

			node->parent = iterator;
			iterator->left = node;
		}
	}

	tree->last = node;
	tree->numelm += 1;
}

void * CBTreeRemove(CBTree* tree) {
	assert(tree->last != NULL);

	void * data = tree->last->data;
	TNode * parent = tree->last->parent;
	free(tree->last);

	if(parent == NULL) {
		tree->root = NULL;
		tree->last = NULL;

	} else if(parent->right != NULL) {
		parent->right = NULL;
		tree->last = parent->left;

	} else {
		TNode* iterator = parent;

		while(iterator->parent && iterator == iterator->parent->left)
			iterator = iterator->parent;

		if(iterator->parent != NULL)
			iterator = iterator->parent->left;

		while(iterator->right)
			iterator = iterator->right;

		parent->left = NULL;
		tree->last = iterator;
	}

	tree->numelm -= 1;
	return data;
}

void CBTreeSwap(CBTree* tree, TNode* parent, TNode* child) {
	assert(parent != NULL && child != NULL && (child == parent->left || child == parent->right));

	TNode* left = child->left;
	TNode* right = child->right;

	if(parent->parent == NULL)
		tree->root = child;
	else if(parent == parent->parent->left)
		parent->parent->left = child;
	else
		parent->parent->right = child;

	if(child == tree->last)
		tree->last = parent;
	else if(left != NULL) {
		left->parent = parent;
		if(right != NULL)
			right->parent = parent;
	}

	if(parent->left != child) {
		parent->left->parent = child;
		child->right = parent;
		child->left = parent->left;
	} else {
		child->left = parent;
		child->right = parent->right;
		if(parent->right != NULL)
			parent->right->parent = child;
	}

	child->parent = parent->parent;
	parent->parent = child;
	parent->left = left;
	parent->right = right;
}

void CBTreeSwapRootLast(CBTree* tree) {
	TNode* root = tree->root;
	TNode* last = tree->last;

	if(root != NULL && root->right != NULL) {
		root->left->parent = last;
		root->right->parent = last;
		last->left = root->left;
		last->right = root->right;
		root->left = NULL;
		root->right = NULL;
		if(last->parent->left == last)
			last->parent->left = root;
		else
			last->parent->right = root;
		root->parent = last->parent;
		last->parent = NULL;
		tree->root = last;
		tree->last = root;

	} else if(root != last)
		CBTreeSwap(tree, root, last);
}