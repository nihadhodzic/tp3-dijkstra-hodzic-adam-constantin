#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"

DTable * newDTable() {
	DTable * dtab = malloc(sizeof(DTable));
	dtab->T = malloc(sizeof(void*));
	dtab->size = 1;
	dtab->used = 0;
	return dtab;
}

/**
 * @brief
 * Dédoubler la taille du tableau dtab
 */
static void scaleUp(DTable *dtab) {
	dtab->size *= 2;
	dtab->T = realloc(dtab->T, dtab->size * sizeof(void*));
	assert(dtab->T != NULL);
}

/**
 * @brief
 * Diviser par 2 la taille du tableau dtab
 */
static void scaleDown(DTable *dtab) {
	dtab->size /= 2;
	dtab->T = realloc(dtab->T, dtab->size * sizeof(void*));
	assert(dtab->T != NULL);
}

void viewDTable(const DTable *dtab, void (*ptrf)(const void*)) {
	int i;
	printf("size = %d\n", dtab->size);
	printf("used = %d\n", dtab->used);
	for (i = 0; i < dtab->used; i++) {
		ptrf(dtab->T[i]);
		printf(" ");
	}
	printf("\n");
}

void DTableInsert(DTable *dtab, void *data) {
	if(dtab->used == dtab->size)
		scaleUp(dtab);

	dtab->T[dtab->used ++] = data;
}

void * DTableRemove(DTable *dtab) {
	assert(dtab->used > 0);

	if(dtab->used == dtab->size / 4)
		scaleDown(dtab);

	return dtab->T[-- dtab->used];
}

void DTableSwap(DTable *dtab, int pos1, int pos2) {
	assert(pos1 < dtab->used);
	assert(pos2 < dtab->used);

	void * temp_ptr = dtab->T[pos1];
	dtab->T[pos1] = dtab->T[pos2];
	dtab->T[pos2] = temp_ptr;
}
