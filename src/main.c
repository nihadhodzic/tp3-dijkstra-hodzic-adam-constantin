#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include "road.h"
#include "graph.h"
#include "test.h"
#include "time.h"

/**
 * Exemple d'une fonction qui affiche le contenu d'un HNode.
 * A modifier si besoin.
 */
void viewHNode(const HNode *node) {
	struct town * town = node->data;
	printf("(%d, %s)", node->value, getTownName(town));
}

/**
 * Affichage de la solution de l'algorithme de Dijkstra.
 * Pour chaque ville du graphe G on doit déjà avoir défini
 * les valeurs dist et pred en exécutant l'algorithme de Dijkstra.
 */
void viewSolution(graph G, char * sourceName) {
	printf("Distances from %s\n", sourceName);
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		if (strcmp(getTownName(iterator->data), sourceName) != 0)
			printf("%s : %d km (via %s)\n", getTownName((struct town *) iterator->data),
											((struct town *) iterator->data)->dist,
											getTownName(((struct town *) iterator->data)->pred));
	}
}

void saveSolution(char* outFileName, graph G, char* sourceName) {
	FILE* fout = fopen(outFileName, "w");
	if(fout == NULL) {
		fprintf(stderr, "err: provided output file '%s' not found or cannot be created.\n", outFileName);
		exit(EXIT_FAILURE);
	}

	fprintf(fout, "%d\n%s\n", G->numelm, sourceName);

	for(LNode* iterator = G->head; iterator; iterator = iterator->suc) {
		struct town * curr = iterator->data;
		if (strcmp(getTownName(curr), sourceName) != 0)
			fprintf(fout, "%s %d %s\n", getTownName(curr),
										curr->dist,
										getTownName(curr->pred));
	}

	fclose(fout);
}

/**
 * Algorithme de Dijkstra
 * inFileName : nom du fichier d'entrée
 * outFileName : nom du fichier de sortie
 * sourceName : nom de la ville de départ
 * heaptype : type du tas {0--tableaux dynamiques, 1--arbres binaires complets, 2--listes ordonnées}
 */
void Dijkstra(char * inFileName, char * outFileName, char * sourceName, int heaptype) {
	Heap* H = newHeap(heaptype);
	graph G = readmap(inFileName);

	FILE* fout = fopen(outFileName, "w");
	if(fout == NULL) {
		fprintf(stderr, "err dijkstra: provided output file '%s' not found or cannot be created.\n", outFileName);
		exit(EXIT_FAILURE);
	}

	struct town * departure = NULL;

	LNode* iterator;
  	for(iterator = G->head; iterator; iterator = iterator->suc) {
  		struct town * curr = iterator->data;
  		if(strcmp(getTownName(curr), sourceName) != 0)
  			curr->ptr = H->HeapInsert(H, INFTY, curr);
  		else {
			curr->ptr = NULL;
			curr->dist = 0;
			departure = curr;
  		}
  	}

	if(departure == NULL) {
		fprintf(stderr, "err dijkstra: provided source '%s' doesn't match any of sources in '%s'\n", sourceName, inFileName);
		exit(EXIT_FAILURE);
	}

	for(int i = 1; i != G->numelm; i++) {
		for(iterator = departure->alist->head; iterator; iterator = iterator->suc) {
			struct town * u = getURoad(iterator->data);
			struct town * v = getVRoad(iterator->data);
			struct town * curr = (departure == u ? v : u);

			if(curr->ptr != NULL) {
				int dist = departure->dist + getKM(iterator->data);
				if(dist < curr->dist) {
					curr->dist = dist;
					curr->pred = departure;
					H->HeapIncreasePriority(H, curr->ptr, dist);
				}
			}
  		}

		HNode * extracted = H->HeapExtractMin(H);
  		departure = extracted->data;
		departure->ptr = NULL;
		free(extracted);
  	}

	saveSolution(outFileName, G, sourceName);

	freeGraph(G);
	free(H->heap);
	if(heaptype == 0)
		free(H->dict);

	free(H);
}

int main() {
	Dijkstra("data/map2", "data/out", "Metz", 2);
	
	return EXIT_SUCCESS;
}
